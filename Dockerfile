# Imangen base
FROM node:latest

# Directorio de la app
WORKDIR /app

# Copiado de archivos
ADD build/v3 /app/build/v3
ADD server.js /app
ADD package.json /app

# Dependencias
RUN npm install

# Puerto que expongo
EXPOSE 3000

# Comando
CMD ["npm","start"]
